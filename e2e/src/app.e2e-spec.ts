import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('Swansky App mobile', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    browser.manage().window().setSize(320, 1050);
  });

  it('should display logo', () => {
    page.navigateTo();
    expect(page.getLogo()).toBeTruthy();
  });

  it('should open menu on click', () => {
    page.clickMenu();
    expect(page.getMenu()).toBeTruthy();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
