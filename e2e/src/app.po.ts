import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getLogo() {
    return element(by.css('.header-logo'));
  }

  clickMenu() {
    return element(by.css('.menu-container.visible-mobile')).click();
  }

  getMenu() {
    return element(by.css('.menu-list-item'));
  }
}
