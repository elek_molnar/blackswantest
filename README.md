# BlackSwanTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

The Test

Build the responsive design from the designs found in the "bs-bud-sitebuild" folder. For the images use your choice of placeholder service, we used https://placekitten.com/

Please don’t use any CSS framework
The main evaulation points: responsivity, clean and readable html and css code, structure
For breakpoints, bootstrap 3 is a good guide
Not required: Animations
Bonus: Git history
