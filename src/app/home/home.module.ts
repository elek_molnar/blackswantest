import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from "./home.component";
import {ImageListComponent} from "../image-list/image-list.component";
import {PostsComponent} from "../posts/posts.component";
import {HomeRoutingModule} from "./home-routing.module";
import {CommonComponentsModule} from "../shared/module/common-components.module";

@NgModule({
  declarations: [
    HomeComponent,
    ImageListComponent,
    PostsComponent,
  ],
  imports: [
    HomeRoutingModule,
    CommonModule,
    CommonComponentsModule
  ]
})
export class HomeModule {
}
