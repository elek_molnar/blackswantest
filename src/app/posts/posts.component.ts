import {Component, OnInit} from '@angular/core';
import {PostsModel} from "./posts.model";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: PostsModel[];

  constructor() {
    this.posts = [
      {
        title: 'Lorem Ipsum',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut tincidunt ante. Maecenas vitae ante at quam pretium fermentum eget placerat augue. Curabitur at tortor vehicula, molestie neque ut, dictum leo. Aenean mi magna, fermentum nec sapien et.'
      },
      {
        title: 'Lorem Ipsum',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      },
      {
        title: 'Lorem Ipsum',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
      }
    ]
  }

  ngOnInit() {
  }

}
