import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PostsComponent} from './posts.component';
import {PostsModel} from "./posts.model";

describe('PostsComponent', () => {
  let component: PostsComponent;
  let fixture: ComponentFixture<PostsComponent>;
  let testPosts: PostsModel[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display posts', () => {
    const postLenght = 2;
    testPosts = [
      {
        title: 'first post',
        content: 'first post content'
      },
      {
        title: 'second post',
        content: 'second post content'
      }
    ];
    component.posts = testPosts;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelectorAll('.posts').length).toEqual(postLenght);
    expect(fixture.nativeElement.querySelectorAll('.post-title')[0].innerText).toEqual(testPosts[0].title);
    expect(fixture.nativeElement.querySelectorAll('.post-content')[0].innerText).toEqual(testPosts[0].content);
  });
});
