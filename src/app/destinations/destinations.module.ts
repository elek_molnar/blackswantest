import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DestinationsRoutingModule} from "./destinations-routing.module";
import {DestinationsComponent} from "./destinations.component";
import {CommonComponentsModule} from "../shared/module/common-components.module";

@NgModule({
  declarations: [
    DestinationsComponent,
  ],
  imports: [
    CommonModule,
    DestinationsRoutingModule,
    CommonComponentsModule
  ]
})
export class DestinationsModule {
}
