export interface NavigationModel {
  url: string,
  name: string
}
