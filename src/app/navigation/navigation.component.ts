import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NavigationModel} from "./navigation.model";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Output() menuToggled: EventEmitter<boolean> = new EventEmitter();
  menuList: NavigationModel[];
  toggle: boolean;

  constructor() {
    this.toggle = false;
    this.menuList = [
      {
        url: "/home",
        name: "Home"
      },
      {
        url: "/destinations",
        name: "Destinations"
      },
      {
        url: "/entertainment",
        name: "Entertainment"
      },
      {
        url: "/news",
        name: "News"
      },
      {
        url: "/weather",
        name: "Weather"
      },
      {
        url: "/shopping",
        name: "Shopping"
      }
    ]
  }

  ngOnInit() {
  }
}
