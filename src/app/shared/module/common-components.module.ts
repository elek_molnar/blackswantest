import {NgModule} from '@angular/core';

import {BannerComponent} from "../../banner/banner.component";
import {InfoTextComponent} from "../../info-text/info-text.component";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    BannerComponent,
    InfoTextComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [BannerComponent, InfoTextComponent]
})
export class CommonComponentsModule {
}
