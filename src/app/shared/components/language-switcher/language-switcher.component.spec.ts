import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LanguageSwitcherComponent} from './language-switcher.component';

describe('LanguageSwitcherComponent', () => {
  let component: LanguageSwitcherComponent;
  let fixture: ComponentFixture<LanguageSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LanguageSwitcherComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display language input', () => {
    const testLang = 'test input';
    component.lang = testLang;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('.lang-type').innerText).toEqual(testLang);
    expect(component).toBeTruthy();
  });
});
