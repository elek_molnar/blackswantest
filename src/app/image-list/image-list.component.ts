import {Component, OnInit} from '@angular/core';
import {ImageListModel} from "./image-list.model";

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {
  imageList: ImageListModel[];
  imageLoaded: boolean;

  constructor() {
    this.imageLoaded = false;
    this.imageList = [
      {
        url: 'https://placekitten.com/700/600'
      },
      {
        url: 'https://placekitten.com/600/600'
      }
    ]
  }

  ngOnInit() {
  }

  imageOnLoad() {
    this.imageLoaded = true;
  }
}
