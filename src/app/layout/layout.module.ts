import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LayoutRoutingModule} from './layout-routing.module';
import {HeaderComponent} from "../header/header.component";
import {FooterComponent} from "../footer/footer.component";
import {LayoutComponent} from "./layout.component";
import {NavigationComponent} from "../navigation/navigation.component";
import {LanguageSwitcherComponent} from "../shared/components/language-switcher/language-switcher.component";

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    NavigationComponent,
    FooterComponent,
    LanguageSwitcherComponent
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule
  ]
})
export class LayoutModule {
}
